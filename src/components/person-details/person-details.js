import React, { useContext, useState, useEffect } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import {NameContext} from '../app/app';

import './person-details.css';

const PersonDetails = () => {
const[information, setInformation] = useState([])
console.warn(information);
const {profileInformation} = useContext(NameContext);
const kaysProfileInformation = Object.keys(profileInformation);
useEffect(() => {
  setInformation(profileInformation)
},[profileInformation])
    return (
      <div className="person-details card">
        <img className="person-image"
          /* src = {profileInformation.url} */
          src="https://starwars-visualguide.com/assets/img/characters/3.jpg" />

        <div className="card-body">
          <h4>{profileInformation.name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">{profileInformation === false ? <CircularProgress /> : kaysProfileInformation[7]}</span>
              <span>{profileInformation[kaysProfileInformation[7]]}</span>
            </li>
            <li className="list-group-item">
              <span className="term">{profileInformation === false ? <CircularProgress /> : kaysProfileInformation[6]}</span>
              <span>{profileInformation[kaysProfileInformation[6]]}</span>
            </li>
            <li className="list-group-item">
              <span className="term">{profileInformation === false ? <CircularProgress /> : kaysProfileInformation[5]}</span>
              <span>{profileInformation[kaysProfileInformation[5]]}</span>
            </li>
          </ul>
        </div>
      </div>
    )
  }

export default PersonDetails;